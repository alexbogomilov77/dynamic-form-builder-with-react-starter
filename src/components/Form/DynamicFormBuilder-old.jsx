import "./DynamicFormBuilder.css";
import React, { Component } from "react";
import Text from "./Fields/Text";
import Select from "./Fields/Select";
import Checkbox from "./Fields/Checkbox";

class DynamicFormBuilder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {},
      formReady: true,
    };
  }
  render() {
    return (
      <>
        <form className="row g-3" onSubmit={this.handleSubmit}>
          {this.fields()}
          <div className="col-12">
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </div>
        </form>
      </>
    );
  }
  fields() {
    return (
      <ul>
        {this.props.json.map((field) => (
          <li
            key={field.id}
            className={`
            field
            ${
              field.id === "email" && !this.state.formData.subscribe
                ? "disabled"
                : "enabled"
            }
            ${
              !this.state.formReady &&
              (!this.state.formData[field.id] ||
                this.state.formData[field.id].length === 0) &&
              field.required
                ? "warning"
                : "null"
            }
          `}
          >
            {field.type === "text" && (
              <Text
                id={field.id}
                type={field.type}
                label={field.label}
                placeholder={field.placeholder}
                isSubscribed={this.state.subs}
                handleInput={this.handleInput}
              />
            )}
            {field.type === "select" && (
              <Select
                id={field.id}
                type={field.type}
                options={field.options}
                placeholder={field.placeholder}
                handleInput={this.handleInput}
              />
            )}
            {field.type === "checkbox" && (
              <Checkbox
                id={field.id}
                type={field.type}
                label={field.label}
                handleCheckbox={this.handleCheckbox}
              />
            )}
          </li>
        ))}
      </ul>
    );
  }

  componentDidMount() {
    this.props.json.forEach((field) => {
      this.setState((prevState) => ({
        formData: { ...prevState.formData, [field.id]: null },
      }));
    });
  }

  handleInput = (e) => {
    const { id, value } = e.target;
    this.setState((prevState) => ({
      formData: { ...prevState.formData, [id]: value },
    }));
  };

  handleCheckbox = (e) => {
    const { id, checked } = e.target;
    this.setState((prevState) => ({
      formData: { ...prevState.formData, [id]: checked },
    }));
  };

  ifErrors = () => {
    let error;
    for (const field in this.state.formData) {
      if (this.state.formData[field] === null || "") {
        error = true;
        break;
      }
    }

    return error;
  };

  handleSubmit = (e) => {
    e.preventDefault();
    if (this.ifErrors()) {
      this.setState((prevState) => ({
        ...prevState,
        formReady: false,
      }));
    } else {
      this.setState((prevState) => ({
        ...prevState,
        formReady: true,
      }));
      console.log("data is ready.", this.state.formData);
    }
  };
}

export default DynamicFormBuilder;
