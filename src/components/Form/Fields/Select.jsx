import React from "react";

const Select = (props) => {
  const { id, options } = props.field;

  return (
    <div className="col-md-4">
      <label htmlFor={id} className="form-label">
        Country
      </label>
      <select
        className="form-select"
        id={id}
        onChange={(e) => props.handleInput(e)}
      >
        <option disabled selected value>
          Choose..
        </option>
        {options.map((option) => (
          <option key={option.label} value={option.label}>
            {option.label}
          </option>
        ))}
      </select>
    </div>
  );
};

export default Select;
