import React from "react";

const Text = (props) => {
  const { id, type, label, placeholder } = props.field;

  return (
    <div className="col-md-6">
      <label className="form-label" htmlFor={id}>
        {label}
      </label>
      <input
        className="form-control"
        id={id}
        type={type}
        placeholder={placeholder}
        onChange={(e) => props.handleInput(e)}
      />
    </div>
  );
};

export default Text;
