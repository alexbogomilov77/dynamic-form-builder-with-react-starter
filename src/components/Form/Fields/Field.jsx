import React from "react";
import InputTypes from "./InputTypes";

const Field = (props) => {
  const { field, handleInput } = props;

  const type = field.type.charAt(0).toUpperCase() + field.type.slice(1);
  const Input = InputTypes[type];

  return <Input field={field} handleInput={handleInput} />;
};

export default Field;
