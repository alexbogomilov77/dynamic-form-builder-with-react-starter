import Text from "./Text";
import Select from "./Select";
import Checkbox from "./Checkbox";

export default {
  Text,
  Select,
  Checkbox,
};
