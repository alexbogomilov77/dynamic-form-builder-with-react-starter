import React from "react";

const Checkbox = (props) => {
  const { id, type, label } = props.field;
  // const { handleInput } = props.handleInput;
  // console.log('props', props)
  // console.log('props.handleInput', props.handleInput)
  // console.log('handleInput', handleInput)

  return (
    <div className="col-12">
      <div className="form-check">
        <input
          className="form-check-input"
          id={id}
          type={type}
          onChange={(e) => props.handleInput(e)}
        />
        <label className="form-check-label" htmlFor={id}>
          {label}
        </label>
      </div>
    </div>
  );
};

export default Checkbox;
