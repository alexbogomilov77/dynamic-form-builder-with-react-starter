import "./DynamicFormBuilder.css";
import React, { useState, useEffect } from "react";
import Field from "./Fields/Field";

const DynamicFormBuilderHOOKS = (props) => {
  const { data } = props;
  const [formData, setformData] = useState({});
  const [formReady, setformReady] = useState(true);

  useEffect(() => {
    data.forEach((field) => {
      setformData((prevState) => ({
        ...prevState,
        [field.id]: null,
      }));
    });
  }, []);

  const handleInput = (e) => {
    setformData((prevState) => ({
      ...prevState,
      [e.target.id]: `${
        e.target.type === "checkbox" ? e.target.checked : e.target.value
      }`,
    }));
  };

  const ifErrors = () => {
    let error;
    for (const field in formData) {
      if (formData[field] === null || "") {
        error = true;
        break;
      }
    }
    return error;
  };

  const toggleEnableClass = (fieldId) => {
    return fieldId === "email" && !formData.subscribe ? "disabled" : "enabled";
  };

  const toggleWarningClass = (field) => {
    return !formReady &&
      (!formData[field.id] || formData[field.id].length === 0) &&
      field.required
      ? "warning"
      : "null";
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (ifErrors()) {
      setformReady(false);
    } else {
      setformReady(true);
      console.log("data is ready.", formData);
    }
  };

  const fields = () =>
    data.map((field) => {
      return (
        <li
          key={field.id}
          className={`field ${toggleEnableClass(field.id)} ${toggleWarningClass(field)}`}
        >
          {<Field field={field} handleInput={handleInput} />}
        </li>
      );
    });

  return (
    <>
      <form className="row g-3" onSubmit={handleSubmit}>
        <ul>{fields()}</ul>
        <div className="col-12">
          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </div>
      </form>
    </>
  );
};

export default DynamicFormBuilderHOOKS;
