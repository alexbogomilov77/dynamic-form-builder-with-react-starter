import React from "react";
import { formData } from "./components/Form/formData";
// import DynamicFormBuilder from "./components/Form/DynamicFormBuilder-old";
import DynamicFormBuilderHOOKS from "./components/Form/DynamicFormBuilderHOOKS";
import "./App.css";

function App() {
  return (
    <div className="container">
      <DynamicFormBuilderHOOKS data={formData} />
    </div>
  );
}

export default App;
